package org.acme.reactive.crud;

import io.quarkus.runtime.StartupEvent;
import io.vertx.mutiny.pgclient.PgPool;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class DBInit {
    private final PgPool client;
    private final boolean schemaCreate;

    public DBInit(PgPool client, @ConfigProperty(name = "myapp.schema.create", defaultValue = "true") boolean schemaCreate) {
        this.client = client;
        this.schemaCreate = schemaCreate;
    }

    void onStart(@Observes StartupEvent ev) {
        if (schemaCreate) {
            initdb();
        }
    }

    private void initdb() {
        client.query("DROP TABLE IF EXISTS post").execute()
                .flatMap(r -> client.query("CREATE TABLE post (id SERIAL PRIMARY KEY, title TEXT NOT NULL, content TEXT NOT NULL)").execute())
                .flatMap(r -> client.query("INSERT INTO post (title, content) VALUES ('first post title', 'first post content')").execute())
                .flatMap(r -> client.query("INSERT INTO post (title, content) VALUES ('second post title', 'second post content')").execute())
                .await().indefinitely();
        client.query("DROP TABLE IF EXISTS tag").execute()
                .flatMap(r -> client.query("CREATE TABLE tag (id SERIAL PRIMARY KEY, label TEXT NOT NULL)").execute())
                .flatMap(r -> client.query("insert into tag(label) values ('first tag label')").execute())
                .flatMap(r -> client.query("insert into tag(label) values ('second tag label')").execute())
                .await().indefinitely();
        client.query("DROP TABLE IF EXISTS posttag").execute()
                .flatMap(r -> client.query("CREATE TABLE posttag (id SERIAL PRIMARY KEY, tag_id integer NOT NULL, post_id integer NOT NULL)").execute())
                .flatMap(r -> client.query("insert into posttag(tag_id,post_id) values (1,1)").execute())
                .flatMap(r -> client.query("insert into posttag(tag_id,post_id) values (1,2)").execute())
                .flatMap(r -> client.query("insert into posttag(tag_id,post_id) values (2,1)").execute())
                .flatMap(r -> client.query("insert into posttag(tag_id,post_id) values (2,2)").execute())
                .await().indefinitely();
    }
}
