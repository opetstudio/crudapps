package org.acme.reactive.crud.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.acme.reactive.crud.entity.Post;
import org.acme.reactive.crud.entity.Tag;
import org.eclipse.microprofile.graphql.Name;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Name("PostResponse")
public class TagDTO {
    public Long id;
    public String label;

    public static TagDTO from(Tag tag) {
        return TagDTO
                .builder()
                .id(tag.id)
                .label(tag.label)
                .build();
    }
    public static List<TagDTO> from(List<Tag> tag) {
        return tag.stream().map(TagDTO::from)
                .collect(Collectors.toList());

    }
}
