package org.acme.reactive.crud.entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.smallrye.mutiny.Multi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
@Table(indexes = {
        @Index(name = "post_id_index", columnList = "post_id"),
        @Index(name = "tag_id_index", columnList = "tag_id"),
},
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"tag_id", "post_id"})
        })
@NamedQueries(value = {
        @NamedQuery(name = "PostTag.getByTagId", query = "SELECT c FROM PostTag c JOIN FETCH c.post where c.tag.id = ?1"),
        @NamedQuery(name = "PostTag.getByPostId", query = "SELECT c FROM PostTag c JOIN FETCH c.tag where c.post.id = ?1")
})
@Getter
public class PostTag extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public Post post;

    @OneToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public Tag tag;

    public static Multi<PostTag> getTagByPostQuery(Long postId) {
        return stream("#PostTag.getByPostId", postId);
    }

    public static Multi<PostTag> getPostByTagQuery(Long tagId) {
        return stream("#PostTag.getByTagId", tagId);
    }
    public String toString() {
        return this.getClass().getSimpleName() + "<" + this.id + ">";
    }
}
