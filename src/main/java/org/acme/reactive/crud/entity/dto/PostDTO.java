package org.acme.reactive.crud.entity.dto;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.acme.reactive.crud.entity.Post;
import org.eclipse.microprofile.graphql.Name;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Name("PostResponse")
public class PostDTO {
    public Long id;
    public String title;
    public String content;

    public static PostDTO from(Post post) {
        return PostDTO
                .builder()
                .id(post.id)
                .title(post.title)
                .content(post.content)
                .build();
    }
    public static List<PostDTO> from(List<Post> post) {
        return post.stream().map(PostDTO::from)
                .collect(Collectors.toList());

    }
}
