package org.acme.reactive.crud.entity;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple2;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.util.Collections;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
@Getter
public class Tag extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public String label;
    public static Uni<List<Tag>> getAllTags() {
        return Tag
                .listAll()
                .ifNoItem()
                .after(Duration.ofMillis(10000))
                .fail()
                .onFailure()
                .recoverWithUni(Uni.createFrom().<List<PanacheEntityBase>>item(Collections.EMPTY_LIST));
    }
    public static Uni<Tag> findOneById(Long id) {
        return findById(id);
    }
    public static Uni<Tag> updateTag(Long id, Tag tag) {
        return Panache
                .withTransaction(() -> findOneById(id)
                        .onItem().ifNotNull()
                        .transform(entity -> {
                            entity.label = tag.label;
                            return entity;
                        })
                        .onFailure().recoverWithNull());
    }
    public static Uni<Tag> addTag(Tag tag) {
        return Panache
                .withTransaction(tag::persist)
                .replaceWith(tag)
                .ifNoItem()
                .after(Duration.ofMillis(10000))
                .fail()
                .onFailure()
                .transform(t -> new IllegalStateException(t));
    }
    public static Uni<Boolean> deleteTag(Long id) {
        return Panache.withTransaction(() -> deleteById(id));
    }
    public static Uni<Tag> addOnePostToTag(Long tagId, Long postId) {

        Uni<Tag> tag = findById(tagId);
        Uni<Post> post = Post.findById(postId);

        Uni<Tuple2<Post, Tag>> tagPostUni = Uni.combine()
                .all().unis(post, tag).asTuple();

        return Panache
                .withTransaction(() -> tagPostUni
                        .onItem().ifNotNull()
                        .transform(entity -> {

                            if (entity.getItem2() == null || entity.getItem1() == null) {
                                return null;
                            }
                            return PostTag.builder()
                                    .post(entity.getItem1())
                                    .tag(entity.getItem2()).build();

                        })
                        .onItem().call(postTag -> postTag.persist())
                        .onItem().transform(postTag -> postTag.tag));

    }
    public String toString() {
        return this.getClass().getSimpleName() + "<" + this.id + ">";
    }
}

