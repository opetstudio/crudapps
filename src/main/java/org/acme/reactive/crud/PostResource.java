package org.acme.reactive.crud;

import io.smallrye.mutiny.Uni;
import org.acme.reactive.crud.entity.*;
import org.acme.reactive.crud.entity.dto.PostDTO;
import org.acme.reactive.crud.entity.dto.TagDTO;
import org.acme.reactive.crud.exception.AlreadyExistingException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("post")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostResource {

    @GET
    @Path("/fetchAllPost")
    public Uni<List<PostDTO>> fetchAllPost() {
        return Post.getAllPosts()
                .onItem()
                .transform(PostDTO::from);
    }
    @GET
    @Path("/fetchOnePostById/{id}")
    public Uni<PostDTO> fetchOnePostById(long id) {
        return Post.findOneById(id)
                .onItem()
                .transform(PostDTO::from);
    }
    @GET
    @Path("/fetchAllTagByPostId/{postId}")
    public Uni<List<TagDTO>> fetchAllTagByPostId(long postId) {
        return PostTag.getTagByPostQuery(postId).onItem().transform(postTag ->
                postTag.tag).collect().asList().onItem().transform(TagDTO::from);
    }
    @GET
    @Path("/fetchAllPostByTagId/{tagId}")
    public Uni<List<PostDTO>> fetchAllPostByTagId(Long tagId) {
        return PostTag.getPostByTagQuery(tagId)
                .onItem()
                .transform(postTag -> postTag.post)
                .collect()
                .asList()
                .onItem()
                .transform(PostDTO::from);
    }
    @POST
    @Path("/createOnePost")
    public Uni<PostDTO> createOnePost(Post post) {
        return Post.addPost(post).onItem().transform(PostDTO::from);
    }

    @POST
    @Path("/updateOnePost/{postId}")
    public Uni<PostDTO> updateOnePost(long postId, Post post) {
        return Post.updatePost(postId, post).onItem().transform(PostDTO::from);
    }
    @GET
    @Path("/deleteOnePost/{postId}")
    public Uni<Boolean> deleteOnePost(long postId) {
        return Post.deletePost(postId);
    }

    @GET
    @Path("/addOneTagToPost/{postId}/{tagId}")
    public Uni<PostDTO> addOneTagToPost(long postId, long tagId) {
        return Post.addOneTagToPost(postId, tagId).onItem().transform(PostDTO::from).onFailure().
                transform(throwable -> new AlreadyExistingException("postId: " + postId + " and tagId: " + tagId));
    }
}
