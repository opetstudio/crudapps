package org.acme.reactive.crud;

import io.smallrye.mutiny.Uni;
import org.acme.reactive.crud.entity.*;
import org.acme.reactive.crud.entity.dto.PostDTO;
import org.acme.reactive.crud.entity.dto.TagDTO;
import org.acme.reactive.crud.exception.AlreadyExistingException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("tag")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagResource {

    @GET
    @Path("/fetchAllTag")
    public Uni<List<TagDTO>> fetchAllTag() {
        return Tag.getAllTags()
                .onItem()
                .transform(TagDTO::from);
    }
    @GET
    @Path("/fetchOneTagById/{id}")
    public Uni<TagDTO> fetchOneTagById(long id) {
        return Tag.findOneById(id)
                .onItem()
                .transform(TagDTO::from);
    }
    @GET
    @Path("/fetchAllPostByTagId/{tagId}")
    public Uni<List<PostDTO>> fetchAllPostByTagId(long tagId) {
        return PostTag.getPostByTagQuery(tagId).onItem().transform(postTag ->
                postTag.post).collect().asList().onItem().transform(PostDTO::from);
    }

    @GET
    @Path("/fetchAllTagByPostId/{postId}")
    public Uni<List<TagDTO>> fetchAllTagByPostId(Long postId) {
        return PostTag.getTagByPostQuery(postId)
                .onItem()
                .transform(postTag -> postTag.tag)
                .collect()
                .asList()
                .onItem()
                .transform(TagDTO::from);
    }
    @POST
    @Path("/createOneTag")
    public Uni<TagDTO> createOneTag(Tag tag) {
        return Tag.addTag(tag).onItem().transform(TagDTO::from);
    }
    @POST
    @Path("/updateOneTag/{tagId}")
    public Uni<TagDTO> updateOneTag(long tagId, Tag tag) {
        return Tag.updateTag(tagId, tag).onItem().transform(TagDTO::from);
    }
    @GET
    @Path("/deleteOneTag/{tagId}")
    public Uni<Boolean> deleteOneTag(long tagId) {
        return Tag.deleteTag(tagId);
    }
    // add post to tag
    @GET
    @Path("/addOnePostToTag/{tagId}/{postId}")
    public Uni<TagDTO> addOnePostToTag(long tagId, long postId) {
        return Tag.addOnePostToTag(tagId, postId).onItem().transform(TagDTO::from).onFailure().
                transform(Exception::new);
//                transform(throwable -> new AlreadyExistingException("postId: " + postId + " and tagId: " + tagId));
    }
}
